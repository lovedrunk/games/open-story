static func load_dictionary_from_json(filename):
	"""Loads a dictionary from a JSON file"""
	var dictionary = {}
	var file = File.new()
	
	file.open(filename, file.READ)
	dictionary = parse_json(file.get_as_text())
	file.close()

	return dictionary

static func save_dictionary_to_json(dictionary, filename):
	"""Stores a dictionary as a JSON file"""
	var file = File.new()
	file.open(filename, File.WRITE)
	file.store_line(to_json(dictionary))
	file.close()

static func save_dictionary_to_gd_constant(dictionary, constant_name, filename):
	"""Stores a dictionary as a JSON file"""
	var file = File.new()
	file.open(filename, File.WRITE)
	file.store_line("const " + constant_name + " = " + to_json(dictionary))
	file.close()

static func deep_copy_array(array):
	"""Returns a deep copy of an array. WARNING: NOT EFFICIENT"""
	var new_array = []
	for element in array:
		if typeof(element) == TYPE_ARRAY:
			new_array.append(deep_copy_array(element))
		elif typeof(element) == TYPE_DICTIONARY:
			new_array.append(deep_copy_dictionary(element))
		else:
			new_array.append(element)
	return new_array

static func deep_copy_dictionary(dictionary):
	"""Returns a deep copy of a dictionary. WARNING: NOT EFFICIENT"""
	var new_dictionary = {}
	for key in dictionary:
		if typeof(dictionary[key]) == TYPE_ARRAY:
			new_dictionary[key] = deep_copy_array(dictionary[key])
		elif typeof(dictionary[key]) == TYPE_DICTIONARY:
			new_dictionary[key] = deep_copy_dictionary(dictionary[key])
		else:
			new_dictionary[key] = dictionary[key]
	return new_dictionary

static func modify_array(original, modifications):
	"""Adds the modification elements to the original array"""
	for element in modifications:
		original.push_back(element)

static func modify_dictionary(original, modifications):
	"""
	Modifies a dictionary with matching keyvalue modifications

	Throwing an error when keyvalue doesn't exist is intentional design choice to know when things go wrong

	Noting similarities with deep copy, I'm sure these can be combined

	TODO:
		- currently the array modification only adds stuff. This needs to change at some point
		  (so things can also be removed from unit archetypes!)
		- idea after a long time -> rename this to add_to_dictionary
		  and make a separate remove_from_dictionary or so
	"""
	for key in modifications:
		# if not original.has(key):
		# 	print("invalid modification key: " + key)
		# 	return
		if typeof(modifications[key]) == TYPE_ARRAY:
			modify_array(original[key], modifications[key])
		elif typeof(modifications[key]) == TYPE_DICTIONARY:
			modify_dictionary(original[key], modifications[key])
		else:
			original[key] = modifications[key]

# TESTS

static func assert_test(test, condition):
	print('\t' + test)
	if condition:
		print('\t\tV test successful V')
	else:
		print('\t\tX test failed X')

static func run_tests():
	"""runs the dict util unit tests"""
	test_load_dictionary_from_json()
	test_save_dictionary_to_json()
	test_deep_copy_dictionary()

static func test_deep_copy_dictionary():
	"""Tests the dictionary from JSON load"""
	print("running test_deep_copy_dictionary")

	var json_path = "res:///src/util/test/test_json_load.json"
	var dictionary = load_dictionary_from_json(json_path)
	var other_dictionary = deep_copy_dictionary(dictionary)

	other_dictionary.number = 2
	assert_test("testing number change", other_dictionary.number != dictionary.number)
	other_dictionary.string = "meingott"
	assert_test("testing string change", other_dictionary.string != dictionary.string)
	other_dictionary.dict.contents = "newdictcontents"
	assert_test("testing dict contents change", other_dictionary.dict.contents != dictionary.dict.contents)
	other_dictionary.array[0] = "newarraycontents"
	assert_test("testing array contents change", other_dictionary.array[0] != dictionary.array[0])


static func test_load_dictionary_from_json():
	"""Tests the dictionary from JSON load"""
	print("running test_load_dictionary_from_json")

	var json_path = "res:///src/util/test/test_json_load.json"
	var dictionary = load_dictionary_from_json(json_path)

	assert_test("testing dictionary load", dictionary != null)

static func test_save_dictionary_to_json():
	"""Tests the dictionary to JSON save"""
	print("running test_save_dictionary_to_json")
	var json_path = "res:///src/util/test/test_json_load.json"
	var save_path = "res:///src/util/test/test_json_save.json"

	var loaded_dictionary = load_dictionary_from_json(json_path)
	save_dictionary_to_json(loaded_dictionary, save_path)
	var saved_dictionary = load_dictionary_from_json(save_path)
	
	assert_test("testing equal size of keys in original and saved",
		loaded_dictionary.keys().size() == saved_dictionary.keys().size())
	
