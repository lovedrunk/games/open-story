static func is_left_mouse_pressed(event):
	return event is InputEventMouseButton && event.button_index == 1 && event.is_pressed()

static func is_mouse_move(event):
	return event is InputEventMouseMotion

static func is_right_mouse_pressed(event):
	return event is InputEventMouseButton && event.button_index == 2 && event.is_pressed()