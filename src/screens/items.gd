extends Node2D

func _ready():
	for child in get_children():
		child.connect("started", self, "_on_item_started")


func _on_item_started(started_item):
	for item in get_children():
		if item != started_item:
			item.stop()
