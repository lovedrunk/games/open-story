extends Label

onready var audio_player = get_node("AudioStreamPlayer")

# Text to show on the screen
export(String) var text_to_show = "Lorem Ipsum" 

# characters per second
export var show_speed = 10

export(AudioStream) var audio_piece

var characters_shown = 0
var start_time = 0

func _ready():
	set_process(false)
	audio_player.set_stream(audio_piece)
	
func start():
	audio_player.play()
	set_process(true)
	
func reset():
	start_time = 0
	text = ""
	audio_player.stop()

func stop():
	set_process(false)
	

func _process(delta):
	start_time += delta
	var shown_characters = start_time * show_speed
	text = text_to_show.left(shown_characters)
	if shown_characters >= text_to_show.length():
		stop()
	
	
	
	
