extends Node2D

const MOUSE_UTIL = preload("res://src/util/mouse_util.gd")

onready var area = get_node("Area2D")
onready var text = get_node("StoryText")


var mouse_inside = false

# var in_progress = false

signal started(item)

func _ready():
	set_process_input(false)
	area.connect("mouse_entered", self, "_on_mouse_entered")
	area.connect("mouse_exited", self, "_on_mouse_exited")


func _on_mouse_entered():
	mouse_inside = true
	set_process_input(true)


func _on_mouse_exited():
	mouse_inside = false
	set_process_input(false)
	
func clicked():
	text.reset()
	text.start()
	emit_signal("started", self)
	

func stop():
	text.stop()
	text.reset()
	
	
func _input(event):
	if MOUSE_UTIL.is_left_mouse_pressed(event):
		clicked()